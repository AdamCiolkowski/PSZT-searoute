package eiti.pszt.utils;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Utils {

    public static double randomInRange(double a, double b, Random r) {
        if (a > b)
            throw new IllegalArgumentException("Lower bound greater than upper bound: " + a + " > " + b);
        return a + (b - a) * r.nextDouble();
    }

    public static int randomInRange(int a, int b, Random r) {
        if (a > b)
            throw new IllegalArgumentException("Lower bound greater than upper bound: " + a + " > " + b);
        return a + r.nextInt(b);
    }

    public static <E> List<E> concat(List<E> list1, List<E> list2) {
        List<E> list = new ArrayList<>();
        list.addAll(list1);
        list.addAll(list2);
        return list;
    }
}
