package eiti.pszt.gui;

import eiti.pszt.model.Path;
import eiti.pszt.model.Point;

import javax.swing.JPanel;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.RenderingHints;
import java.awt.geom.Arc2D;
import java.awt.geom.Line2D;
import java.awt.geom.Rectangle2D;
import java.awt.geom.RectangularShape;
import java.util.Iterator;
import java.util.List;

import static java.lang.Math.min;

public class PathPanel extends JPanel {

    private final Model model;
    private final RectangularShape area;

    public PathPanel(Model model) {
        this.model = model;
        this.area = area(model);
    }

    private RectangularShape area(Model model) {
        Point source = model.getSource();
        Point goal = model.getGoal();
        return new Rectangle2D.Double(source.getX(), source.getY(), goal.getX(), goal.getY());
    }

    @Override
    public void paint(Graphics g) {
        super.paint(g);
        draw((Graphics2D) g);
    }

    private void draw(Graphics2D g) {
        prepareView(g);
        drawViews(g);
    }

    private void prepareView(Graphics2D g) {
        g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        scaleToFit(g, area);
        centerView(g, area);
    }

    private void scaleToFit(Graphics2D g, RectangularShape r) {
        Rectangle bounds = g.getClipBounds();
        double widthRatio = bounds.getWidth() / r.getWidth();
        double heightRatio = bounds.getHeight() / r.getHeight();
        double scale = 0.9 * min(widthRatio, heightRatio);
        g.scale(scale, scale);
    }

    private void centerView(Graphics2D g, RectangularShape r) {
        Rectangle bounds = g.getClipBounds();
        double cx = (bounds.getWidth() - r.getWidth()) / 2;
        double cy = (bounds.getHeight() - r.getHeight()) / 2;
        g.translate(cx, cy);
    }

    private void drawViews(Graphics2D g) {
        g.draw(area);

        drawPath(g, model.getPath());
    }

    private void drawPath(Graphics2D g, Path path) {
        List<Point> points = path.getPoints();
        drawLines(g, points);
        drawPoints(g, points);
    }

    private void drawLines(Graphics2D g, List<Point> points) {
        if(points.size() < 2)
            return;
        Iterator<Point> it = points.iterator();
        Point first = it.next();
        while (it.hasNext()) {
            Point second = it.next();
            drawLine(g, first, second);
            first = second;
        }
    }

    private void drawPoints(Graphics2D g, List<Point> points) {
        for (Point p : points) {
            drawPoint(g, p);
        }
    }

    private void drawPoint(Graphics2D g, Point p) {
        int r = 7;
        g.draw(new Arc2D.Double(p.getX() - r, p.getY() - r, 2 * r, 2 * r, 0, 360, Arc2D.OPEN));
    }

    private void drawLine(Graphics2D g, Point first, Point second) {
        g.setColor(Color.BLACK);
        g.draw(new Line2D.Double(first.getX(), first.getY(), second.getX(), second.getY()));
    }
}
